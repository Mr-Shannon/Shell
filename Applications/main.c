#include "drv_usart.h"
#include "shell.h"

int TEST(int argc, char *const argv[])
{
  while(argc--)
  {
      printf("argv:%s\r\n", argv[argc]);
  }
  return 0;
}

int main(void)
{
  gd32_usart_init();
  shell_init();

  while(1)
  {
    shell_main_loop("Shell>>");
  }
}
SHELL_EXPORT_CMD(TEST, test, test shell.);
