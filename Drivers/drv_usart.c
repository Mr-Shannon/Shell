#include "drv_usart.h"
#include "stdio.h"

/**
 * This is GD32F10x usart device initial.
 *
 */
void gd32_usart_init(void)
{
  GPIO_InitPara GPIO_InitStruct;
  USART_InitPara USART_InitStruct;

  /* Enable USART GPIO clocks */
  RCC_APB2PeriphClock_Enable(USART1_TX_RCC | USART1_RX_RCC, ENABLE);
  /* Enable USART clock */
  RCC_APB2PeriphClock_Enable(RCC_APB2PERIPH_USART1, ENABLE);
  /* Configure USART Rx PIN */
  GPIO_InitStruct.GPIO_Speed = GPIO_SPEED_50MHZ;
  GPIO_InitStruct.GPIO_Mode = GPIO_MODE_IN_FLOATING;
  GPIO_InitStruct.GPIO_Pin = USART1_RX_PIN;
  GPIO_Init(USART1_RX_PORT, &GPIO_InitStruct);
  /* Configure USART Tx PIN */
  GPIO_InitStruct.GPIO_Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.GPIO_Pin = USART1_TX_PIN;
  GPIO_Init(USART1_TX_PORT, &GPIO_InitStruct);
  /* Configure USART1 */
  USART_DeInit(USART1);
  USART_InitStruct.USART_BRR = 115200;
  USART_InitStruct.USART_WL = USART_WL_8B;
  USART_InitStruct.USART_STBits = USART_STBITS_1;
  USART_InitStruct.USART_Parity = USART_PARITY_RESET;
  USART_InitStruct.USART_HardwareFlowControl = USART_HARDWAREFLOWCONTROL_NONE;
  USART_InitStruct.USART_RxorTx = USART_RXORTX_RX | USART_RXORTX_TX;
  USART_Init(USART1, &USART_InitStruct);
  /* Enable USART */
  USART_Enable(USART1, ENABLE);
}

int fputc(int ch, FILE *f)
{
	USART_DataSend(USART1, (u8) ch);
	while (USART_GetBitState(USART1, USART_FLAG_TBE) == RESET);	
	
	return (ch);
}


int fgetc(FILE *f)
{
	while (USART_GetBitState(USART1, USART_FLAG_RBNE) == RESET);
	return (int)USART_DataReceive(USART1);
}
