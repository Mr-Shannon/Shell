#ifndef __DRV_USART_H__
#define __DRV_USART_H__

#include "gd32f10x.h" 

/* USART1 */
#define USART1_TX_RCC					          RCC_APB2PERIPH_GPIOA
#define USART1_TX_PORT                  GPIOA
#define USART1_TX_PIN					          GPIO_PIN_9

#define USART1_RX_RCC					          RCC_APB2PERIPH_GPIOA
#define USART1_RX_PORT       	          GPIOA
#define USART1_RX_PIN					          GPIO_PIN_10

/* USART2 */
#define USART2_TX_RCC					          RCC_APB2PERIPH_GPIOA
#define USART2_TX_PORT                  GPIOA
#define USART2_TX_PIN					          GPIO_PIN_2

#define USART2_RX_RCC					          RCC_APB2PERIPH_GPIOA
#define USART2_RX_PORT       	          GPIOA
#define USART2_RX_PIN					          GPIO_PIN_3

/* USART3_REMAP[1:0] = 00 */
#define USART3_TX_RCC					          RCC_APB2PERIPH_GPIOB
#define USART3_TX_PORT                  GPIOB
#define USART3_TX_PIN					          GPIO_PIN_10

#define USART3_RX_RCC					          RCC_APB2PERIPH_GPIOB
#define USART3_RX_PORT       	          GPIOB
#define USART3_RX_PIN					          GPIO_PIN_11

void gd32_usart_init(void);
#endif /* __DRV_USART_H__ */
